﻿/*
	Changes made in july development
*/

USE [Wabo]
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SystemSettings ADD
	smtp_server nvarchar(150) NULL,
	smtp_port int NULL,
	smtp_enable_ssl bit NOT NULL,
	smtp_user_name nvarchar(200) NULL,
	smtp_password nvarchar(200) NULL
GO

ALTER TABLE dbo.SystemSettings ADD CONSTRAINT
	DF_SystemSettings_smtp_enable_ssl_1 DEFAULT 0 FOR smtp_enable_ssl

GO

ALTER TABLE dbo.SystemSettings SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



/****** Object:  Table [dbo].[Messages]    Script Date: 07/23/2013 13:15:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Messages](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[from_user] [bigint] NOT NULL,
	[to_user] [bigint] NOT NULL,
	[message_title] [nvarchar](100) NOT NULL,
	[message_body] [ntext] NULL,
	[send_date] [datetime] NOT NULL,
	[parent_message] [bigint] NULL,
 CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_FromUser] FOREIGN KEY([from_user])
REFERENCES [dbo].[Users] ([id])
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_FromUser]
GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_Parent] FOREIGN KEY([parent_message])
REFERENCES [dbo].[Messages] ([id])
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_Parent]
GO

ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_ToUser] FOREIGN KEY([to_user])
REFERENCES [dbo].[Users] ([id])
GO

ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_ToUser]

GO

ALTER TABLE dbo.Messages ADD
	is_read bit NOT NULL CONSTRAINT DF_Messages_is_read DEFAULT 0

GO


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.IpLocks
	(
	ip_address varchar(50) NOT NULL,
	lock_date datetime NOT NULL,
	unlock_date datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.IpLocks ADD CONSTRAINT
	PK_IpLocks PRIMARY KEY CLUSTERED 
	(
	ip_address
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.IpLocks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT