﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using Wabo.Dal.Interface;

namespace Wabo.Dal.Repository
{
    public class WaboEFMessageRepository : EFBaseRepository, IMessageRepository
    {
        public void SendMessage(Message msg)
        {
            try
            {            
                Context.Messages.Add(msg);
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public Message GetMessageById(long messageId)
        {
            return Context.Messages.FirstOrDefault(m => m.id == messageId);
        }

        public IQueryable<Message> GetUnreadMessagesOfUser(User usr)
        {
            return GetUserMessages(usr, false);
        }

        public IQueryable<Message> GetAllMessagesOfUser(User usr)
        {
            var readMessages = GetReadMessagesOfUser(usr);
            var unreadMessages = GetUnreadMessagesOfUser(usr);
            return readMessages.Concat(unreadMessages);
        }

        public IQueryable<Message> GetReadMessagesOfUser(User usr)
        {
            return GetUserMessages(usr, true);
        }

        private IQueryable<Message> GetUserMessages(User usr, bool message_is_read)
        {
            return Context.Messages.Where(m => m.to_user == usr.id && m.is_read == message_is_read); 
        }

        private IQueryable<Message> GetUsersParentMessages(User usr, bool message_is_read)
        {
            return Context.Messages.Where(m => m.parent_message == null && m.to_user == usr.id && m.is_read == message_is_read);
        }

        public IQueryable<Message> GetUnreadRootMessagesOfUser(User usr)
        {
            return GetUsersParentMessages(usr, false);
        }

        public IQueryable<Message> GetAllRootMessagesOfUser(User usr)
        {
            var readMessages = GetReadRootMessagesOfUser(usr);
            var unreadMessages = GetUnreadRootMessagesOfUser(usr);
            return readMessages.Concat(unreadMessages);
        }

        public IQueryable<Message> GetReadRootMessagesOfUser(User usr)
        {
            return GetUsersParentMessages(usr, true);
        }


        public IQueryable<Message> GetSentMessagesOfUser(User usr)
        {
            return Context.Messages.Where(m => m.from_user == usr.id );
        }


        public void MarkMessageAsRead(long messageId)
        {
            try
            {
                Message msg = Context.Messages.Where(m => m.id == messageId).FirstOrDefault();
                if (msg != null)
                {
                    msg.is_read = true;
                    Context.Entry(msg).CurrentValues.SetValues(msg);
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }            

        }

        public void MarkMessageAsUnread(long messageId)
        {
            try
            {
                Message msg = Context.Messages.Where(m => m.id == messageId).FirstOrDefault();
                if (msg != null)
                {
                    msg.is_read = false;
                    Context.Entry(msg).CurrentValues.SetValues(msg);
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }
    }
}
