﻿using NLog;
using System;


namespace Wabo.Dal.Repository
{
    public class EFBaseRepository : IDisposable
    {
        protected WaboEntities Context = new WaboEntities();
        protected Logger Logger = LogManager.GetCurrentClassLogger();



        public void Dispose(bool b)
        {
            Dispose();
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
