﻿using Wabo.Dal.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.Extensions;

namespace Wabo.Dal.Repository
{
    public class WaboEFGroupRepository : EFBaseRepository,IGroupRepository
    {
        

        public Group Get(long groupId)
        {
            try
            {
                return Context.Groups.FirstOrDefault(g => g.id == groupId);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                return null;
            }
        }

        public void Remove(Group g)
        {
            try
            {
                Context.Groups.Remove(g);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public void Remove(long groupId)
        {
            try
            {
                Context.Groups.Remove(Get(groupId));
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public void Update(Group g)
        {
            try
            {
                Group original = Get(g.id);
                if (original != null)
                {
                    Context.Entry(original).CurrentValues.SetValues(g);
                    Context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public void Add(ref Group g)
        {
            try
            {
                Context.Groups.Add(g);
                Context.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public IEnumerable<Group> Groups
        {
            get { return Context.Groups;  }
        }


        public void RemoveAll()
        {
            Context.UserGroups.Delete(ug => true);
            Context.Groups.Delete(g => true);
            Context.SaveChanges();
        }


        public void AddUserToGroup(Group g, User u)
        {
            try
            {
                var original = Context.UserGroups.FirstOrDefault(ug => ug.group_id == g.id && ug.user_id == u.id);
                var userGroup = new UserGroups
                {
                    user_id = u.id,
                    group_id = g.id
                };
                if (original == null)
                {
                    Context.UserGroups.Add(userGroup);
                    Context.SaveChanges();
                }
                
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }


        public void RemoveUserFromGroup(Group g, User u)
        {
            try
            {
                Context.UserGroups.Delete(ug => ug.group_id == g.id && ug.user_id == u.id);
                Context.SaveChanges();                
            }catch(Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }


        public List<User> UsersInGroup(Group g)
        {
            return Context
                    .UserGroups
                    .Where(ug => ug.group_id == g.id)
                    .Select(userGroup => Context.Users.FirstOrDefault(u => u.id == userGroup.user_id)).ToList();
        }
    }
}
