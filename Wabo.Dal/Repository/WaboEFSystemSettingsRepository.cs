﻿using System;
using System.Linq;
using EntityFramework.Extensions;
using Wabo.Dal.Interface;

namespace Wabo.Dal.Repository
{
    public class WaboEFSystemSettingsRepository:EFBaseRepository,ISystemSettingsRepository
    {
        public string ApplicationName
        {
            get
            {
                try
                {
                    return Settings.application_name;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return null;
                }
            }
        }

        public int MaxLoginAttempts
        {
            get { return Settings.max_login_attempts; }
        }

        public int LoginLockDuration
        {
            get { return Settings.login_lock_timeout; }
        }

        public int LogViewerItemCountPerPage
        {
            get { return Settings.log_items_per_page; }
        }

        public bool EnableActiveDirectoryLogin
        {
            get { return Settings.enable_active_directory_login; }
        }

        public SystemSettings Settings
        {
            get { return Context.SystemSettings.FirstOrDefault();}
        }

        public void Save(SystemSettings obj)
        {
            try
            {
                SystemSettings currentSettings = Settings;
                if (currentSettings != null)
                {
                    obj.id = currentSettings.id;
                    Context.Entry(currentSettings).CurrentValues.SetValues(obj);
                }
                else
                {
                    Context.SystemSettings.Add(obj);
                }
                Context.SaveChanges();
            }catch(Exception ex)
            {
                Logger.Error(ex.Message);
                throw;
            }
        }

        public void Clear()
        {
            Context.SystemSettings.Delete(p => true);
        }


        public string Theme
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().theme;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return "Default";
                }
            }
        }





        public int DaysToPurgeLog
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().days_to_purge;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return 30;
                }
            }
        }

        public string SmtpServerName
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().smtp_server;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return String.Empty;
                }
            }
        }

        public string SmtpUserName
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().smtp_user_name;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return String.Empty;
                }
            }
        }

        public string SmtpPassword
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().smtp_password;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return String.Empty;
                }
            }
        }

        public int SmtpPort
        {
            get
            {
                try
                {
                    return (int)Context.SystemSettings.First().smtp_port;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return 25;
                }
            }
        }

        public bool SmtpEnableSSL
        {
            get
            {
                try
                {
                    return Context.SystemSettings.First().smtp_enable_ssl;
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message);
                    return false;
                }
            }
        }
    }
}
