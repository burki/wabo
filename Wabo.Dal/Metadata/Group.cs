﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wabo.Dal
{
    [MetadataType(typeof(GroupMetadata))]
    public partial class Group
    {
        public static void RegisterMetadataBuddy()
        {
            TypeDescriptor.AddProvider(
              new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Group)), typeof(Group)
              );
        }
    }

    public class GroupMetadata
    {
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages),Name="DISP_Name")]
        public string name { get; set; }
        
    }
}
