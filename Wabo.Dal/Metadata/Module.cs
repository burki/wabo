﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wabo.Dal
{
    [MetadataType(typeof(ModuleMetadata))]
    public partial class Module
    {
        public static void RegisterMetadataBuddy()
        {
            TypeDescriptor.AddProvider(
              new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Module)), typeof(Module)
              );
        }
    }

    public class ModuleMetadata
    {
        [Required(ErrorMessageResourceType = typeof(MetadataMessages),ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages),Name = "DISP_Controller")]
        public string controller { get; set; }

        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages),Name="DISP_Title")]
        public string title { get; set; }   
    }
}
