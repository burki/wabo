﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wabo.Dal
{
    [MetadataType(typeof(MessageMetadata))]
    public partial class Message
    {
        public static void RegisterMetadataBuddy()
        {
            TypeDescriptor.AddProvider(
              new AssociatedMetadataTypeTypeDescriptionProvider(typeof(Message)), typeof(Message)
              );
        }
    }

    public class MessageMetadata
    {
       
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_MessageTitle")]
        public string message_title { get; set; }
        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_MessageBody")]
        public string message_body { get; set; }                
        
    }
}
