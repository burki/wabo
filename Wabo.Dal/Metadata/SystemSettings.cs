﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wabo.Dal
{
    [MetadataType(typeof(SystemSettingsMetaData))]
    public partial class SystemSettings
    {
        public static void RegisterMetadataBuddy()
        {
            TypeDescriptor.AddProvider(
              new AssociatedMetadataTypeTypeDescriptionProvider(typeof(SystemSettings)), typeof(SystemSettings)
              );
        }
    }
    public class SystemSettingsMetaData
    {
        [Required(ErrorMessageResourceType = typeof(MetadataMessages),ErrorMessageResourceName = "ERR_Required")]
        [MaxLength(250,ErrorMessageResourceType = typeof(MetadataMessages),ErrorMessageResourceName = "Err_ApplicationNameLength")]
        [Display(ResourceType = typeof(MetadataMessages),Name="DISP_ApplicationName")]
        public string application_name { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_MaxLoginAttempts")]
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]        
        public short max_login_attempts { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_LoginLockDuration")]
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]        
        public int login_lock_timeout { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_LogViewerItemsPerPage")]
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]        
        public int log_items_per_page { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Theme")]        
        public int theme { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_EnableActiveDirectoryLogin")]
        public bool enable_active_directory_login { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_LogPurgeDays")]        
        public int days_to_purge { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_SmtpServerName")]        
        public string smtp_server { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_SmtpServerPort")]        
        public Nullable<int> smtp_port { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_SmtpEnableSsl")]        
        public bool smtp_enable_ssl { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_SmtpUserName")]        
        public string smtp_user_name { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_SmtpPassword")]        
        public string smtp_password { get; set; }

    }
}
