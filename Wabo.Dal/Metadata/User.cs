﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Wabo.Dal
{
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        public static void RegisterMetadataBuddy()
        {
            TypeDescriptor.AddProvider(
              new AssociatedMetadataTypeTypeDescriptionProvider(typeof(User)), typeof(User)
              );
        }
    }

    public class UserMetadata
    {
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [DataType(DataType.EmailAddress,ErrorMessageResourceType = typeof(MetadataMessages),ErrorMessageResourceName="ERR_InvalidMailAddress")]
        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Email")]
        public string email { get; set; }

        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Username")]
        public string username { get; set; }
        
        [Required(ErrorMessageResourceType = typeof(MetadataMessages), ErrorMessageResourceName = "ERR_Required")]
        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Password")]
        [MinLength(5,ErrorMessageResourceType = typeof(MetadataMessages),ErrorMessageResourceName = "ERR_PasswordLength")]
        public string password { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_FirstName")]
        public string name { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_LastName")]
        public string last_name { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Locked")]
        public bool locked { get; set; }

        [Display(ResourceType = typeof(MetadataMessages), Name = "DISP_Domain")]
        public bool domain { get; set; }
    }
}
