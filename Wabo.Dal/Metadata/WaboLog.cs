﻿using System;
using Wabo.Dal.Interface;

namespace Wabo.Dal
{
    public partial class WaboLog
    {
        public String LogOperationDescription
        {
            get 
            { 
                switch (log_operation_type)
                {
                    case (int)LogOperation.Create:
                        return MetadataMessages.LOG_Create;
                    case (int) LogOperation.Delete:
                        return MetadataMessages.LOG_Delete;
                    case (int) LogOperation.Modify:
                        return MetadataMessages.LOG_Modify;
                    case (int) LogOperation.View:
                        return MetadataMessages.LOG_View;
                    case (int) LogOperation.Login:
                        return MetadataMessages.LOG_Login;
                    case (int) LogOperation.Logoff:
                        return MetadataMessages.LOG_Logoff;
                    default:
                        return "-";
                }
            }
        }
    }
}
