﻿using System.Collections.Generic;

namespace Wabo.Dal.Interface
{
    public interface IModuleRepository
    {
        Module Get(long moduleId);
        Module Get(string controllerName);
        void Remove(Module m);
        void Remove(long moduleId);
        void RemoveAll();
        void Update(Module m);
        void Add(ref Module m);
        IEnumerable<Module> Modules { get; }
        bool GroupCanRead(Group grp, Module m);
        bool GroupCanList(Group grp, Module m);
        bool GroupCanWrite(Group grp, Module m);
        bool GroupCanDelete(Group grp, Module m);
        bool UserCanRead(User usr, Module m);
        bool UserCanList(User usr, Module m);
        bool UserCanWrite(User usr, Module m);
        bool UserCanDelete(User usr, Module m);

        void SetModuleGroupPermissions(Group grp, Module m, bool canList, bool canRead, bool canWrite, bool canDelete);

    }
}
