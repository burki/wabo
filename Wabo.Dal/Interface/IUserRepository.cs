﻿using System.Collections.Generic;
using System.Linq;

namespace Wabo.Dal.Interface
{
    public interface IUserRepository
    {
        User Get(long userId);
        User Get(string userName,string domain = "");
        void Remove(User usr);
        void Remove(long userId);
        void Remove(string userName);
        void RemoveAll();
        void Update(User usr);
        void Add(ref User usr);
        IEnumerable<User> Users { get; }       
        //Misc functions        
        bool Validate(string username, string password,string domain = "");
        string CreateHashedPassword(string salt, string password);
        User CreateNewUserObject(string username, string salt, string password, string email);
    }
}
