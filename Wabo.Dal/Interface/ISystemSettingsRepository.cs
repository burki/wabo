﻿
namespace Wabo.Dal.Interface
{
    public interface ISystemSettingsRepository
    {
        void Clear();
        void Save(SystemSettings obj);
        SystemSettings Settings { get; }
        string ApplicationName { get; }
        int MaxLoginAttempts { get; }
        int LoginLockDuration { get; }
        int LogViewerItemCountPerPage { get; }
        bool EnableActiveDirectoryLogin { get; }
        string Theme { get; }
        int DaysToPurgeLog { get; }
        string SmtpServerName { get; }
        string SmtpUserName { get; }
        string SmtpPassword { get; }
        int SmtpPort { get; }
        bool SmtpEnableSSL { get; }
    }
}
