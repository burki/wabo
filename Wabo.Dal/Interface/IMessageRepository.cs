﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wabo.Dal.Interface
{
    public interface IMessageRepository
    {
        void SendMessage(Message msg);
        void MarkMessageAsRead(long messageId);
        void MarkMessageAsUnread(long messageId);
        Message GetMessageById(long messageId);
        IQueryable<Message> GetUnreadMessagesOfUser(User usr);        
        IQueryable<Message> GetAllMessagesOfUser(User usr);
        IQueryable<Message> GetReadMessagesOfUser(User usr);
        IQueryable<Message> GetUnreadRootMessagesOfUser(User usr);
        IQueryable<Message> GetAllRootMessagesOfUser(User usr);
        IQueryable<Message> GetReadRootMessagesOfUser(User usr);
        IQueryable<Message> GetSentMessagesOfUser(User usr);
    }
}
