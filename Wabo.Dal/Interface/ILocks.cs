﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wabo.Dal.Interface
{
    public interface ILocks
    {
        bool IsLocked(string ipAddress);
        void Lock(string ipAddress, int unlockTime /* seconds */ );
        // unlock simply removes ip address from lock table
        void Unlock(string ipAddress); 
    }
}
