﻿using System.Collections.Generic;


namespace Wabo.Dal.Interface
{
    public interface IGroupRepository
    {
        Group Get(long groupId);
        void Remove(Group g);
        void Remove(long groupId);
        void RemoveAll();
        void Update(Group g);
        void Add(ref Group g);
        IEnumerable<Group> Groups{ get; }

        List<User> UsersInGroup(Group g);
        void AddUserToGroup(Group g, User u);
        void RemoveUserFromGroup(Group g, User u);
    }
}
