﻿using System.Linq;

namespace Wabo.Dal.Interface
{
    public enum LogOperation
    {
        View = 10,
        Create = 11,
        Modify = 12,
        Delete = 13,
        Login = 1000,
        Logoff = 1001
    };

    public interface ILogRepository
    {        
        WaboLog Log(User usr, LogOperation operation, string moduleName,string actionName,string description="");
        void RemoveAll();
        void Remove(int days);        
        IQueryable<WaboLog> Logs { get; } 
    }
}
