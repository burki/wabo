//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wabo.Dal
{
    using System;
    using System.Collections.Generic;
    
    public partial class WaboLog
    {
        public long id { get; set; }
        public long user_id { get; set; }
        public System.DateTime date { get; set; }
        public string description { get; set; }
        public int log_operation_type { get; set; }
        public string module { get; set; }
        public string action { get; set; }
    
        public virtual User User { get; set; }
    }
}
