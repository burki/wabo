﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Wabo.Dal;

namespace Wabo.Web
{
    public class MetadataConfig
    {
        public static void RegisterBuddyClasses()
        {
            Module.RegisterMetadataBuddy();
            Group.RegisterMetadataBuddy();
            User.RegisterMetadataBuddy();
            SystemSettings.RegisterMetadataBuddy();
            Message.RegisterMetadataBuddy();            
        }
    }
}