﻿using System.Collections.Generic;

namespace Wabo.Web.Areas.Manager.ViewModels.Acl
{
    public class AclViewModel
    {
        public IEnumerable<AclItem> AclItems { get; set; }    
    }

    public class AclItem
    {        
        public Dal.Group Group { get; set; }
        public Dal.Module Module { get; set; }
        public bool List { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }
        public bool Delete { get; set; }
    }
}