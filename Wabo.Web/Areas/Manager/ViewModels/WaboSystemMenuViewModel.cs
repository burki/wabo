﻿using System.Collections.Generic;

using Wabo.Web.Areas.Manager.Models;

namespace Wabo.Web.Areas.Manager.ViewModels
{
    public class WaboSystemMenuViewModel
    {
        public List<WaboSystemMenuItem> Menu = new List<WaboSystemMenuItem>();   
    }
}