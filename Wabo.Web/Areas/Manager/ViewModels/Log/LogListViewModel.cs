﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wabo.Web.Resources;

namespace Wabo.Web.Areas.Manager.ViewModels.Log
{
    public class LogListViewModel
    {        
        [Display(ResourceType = typeof(Messages), Name = "LogFilterLabelBeginDate")]
        public DateTime FilterStartDate { get; set; }
        [Display(ResourceType = typeof(Messages), Name = "LogFilterLabelEndDate")]
        public DateTime FilterEndDate { get; set; }
        
        [Display(ResourceType = typeof(Messages), Name = "LogFilterLabelModule")]
        public string FilterModule { get; set; }
        public List<SelectListItem> Modules { get; set; }

        [Display(ResourceType = typeof(Messages), Name = "LogFilterLabelUser")]
        public int FilterUserId{ get; set; }
        public List<SelectListItem> Users { get; set; }
    }
}