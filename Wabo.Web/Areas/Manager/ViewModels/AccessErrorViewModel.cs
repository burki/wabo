﻿using System;


namespace Wabo.Web.Areas.Manager.ViewModels
{
    public class AccessErrorViewModel
    {
        public static string TempdataParameterName = "ErrorViewModelTempDataParameter";

        public Exception ErrorException { get; set; }
    }
}