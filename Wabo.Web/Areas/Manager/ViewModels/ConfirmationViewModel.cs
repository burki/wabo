﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wabo.Web.Areas.Manager.ViewModels
{
    public class ConfirmationViewModel
    {
        public string Title { get; set; }
        public string Message { get; set; }
        public string ConfirmationAction { get; set; }
        public string ConfirmationController { get; set; }
        public string CancelAction { get; set; }
        public string CancelController { get; set; }
    }
}