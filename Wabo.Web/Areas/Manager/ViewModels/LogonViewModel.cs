﻿using System;
using System.ComponentModel.DataAnnotations;
using Wabo.Web.Resources;

namespace Wabo.Web.Areas.Manager.ViewModels
{
    public class LogonViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Messages),ErrorMessageResourceName = "ErrorMessageUserNameRequired")]
        [Display(ResourceType = typeof(Messages), Name = "LOGON_Username")]
        public string Username { get; set; }
        [Required(ErrorMessageResourceType = typeof(Messages), ErrorMessageResourceName = "ErrorMessagePasswordRequired")]
        [Display(ResourceType = typeof(Messages), Name = "LOGON_Password")]
        public string Password { get; set; }
        [Display(ResourceType = typeof(Messages), Name = "LOGON_RememberMe")]
        public bool RememberMe { get; set; }
        [Display(ResourceType = typeof(Messages), Name = "LOGON_Domain")]
        public string Domain { get; set; }
    }
}