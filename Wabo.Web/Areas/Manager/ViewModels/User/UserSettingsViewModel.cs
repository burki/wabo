﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Wabo.Web.Areas.Manager.ViewModels.User
{
    public class UserSettingsViewModel
    {
        public string Domain { get; set; }
        [Display(ResourceType = typeof(Dal.MetadataMessages), Name = "DISP_Password")]
        public String Password { get; set; }

        [Required]
        //[EmailAddress]
        [Display(ResourceType = typeof(Dal.MetadataMessages), Name = "DISP_Email")]
        public String Email { get; set; }
        
        [Display(ResourceType = typeof(Dal.MetadataMessages), Name = "DISP_FirstName")]
        public String FirstName { get; set; }
        
        [Display(ResourceType = typeof(Dal.MetadataMessages), Name = "DISP_LastName")]
        public String LastName { get; set; }

        public UserSettingsViewModel() { }
        
        public UserSettingsViewModel(Dal.User usr)
        {
            Email = usr.email;
            FirstName = usr.name;
            LastName = usr.last_name;
            Domain = usr.domain;
        }
        
    }
}