﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Wabo.Web.Resources;

namespace Wabo.Web.Areas.Manager.ViewModels.User
{
    public class ComposeMessageViewModel
    {        
        [Display(ResourceType = typeof(Messages), Name = "DISP_ComposeMessageTo")]
        public long ToUser { get; set; }
        [Display(ResourceType = typeof(Messages), Name = "DISP_ComposeMessageTitle")]
        public string MessageTitle { get; set; }
        [Display(ResourceType = typeof(Messages), Name = "DISP_ComposeMessageBody")]
        public string MessageBody { get; set; }
        public long ParentMessage { get; set; }
        public string NavigationBackUrl { get; set; }
    }
}