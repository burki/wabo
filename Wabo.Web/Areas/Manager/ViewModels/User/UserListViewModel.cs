﻿using System.Collections.Generic;

namespace Wabo.Web.Areas.Manager.ViewModels.User
{
    public class UserListViewModel
    {
        public List<UserListItem> Users = new List<UserListItem>();
        public bool CanWrite { get; set; }
        public bool CanDelete { get; set; }        
    }

    public class UserListItem
    {
        public Dal.User User { get; set; }        
    }
}