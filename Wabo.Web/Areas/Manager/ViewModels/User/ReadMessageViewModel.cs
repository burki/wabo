﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wabo.Web.Areas.Manager.ViewModels.User
{
    public class ReadMessageViewModel
    {
        public bool IsSentMail { get; set; }
        public String FromUser { get; set; }
        public String ToUser { get; set; }
        public String MessageTitle { get; set; }
        public String MessageBody { get; set; }
        public long   MessageId { get; set; }
        public String NavigationBackUrl { get; set; }
    }
}