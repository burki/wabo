﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wabo.Web.Areas.Manager.ViewModels.User
{
    public class UserMessagesViewModel
    {
        public IEnumerable<Dal.Message> Messages { get; set; }
        public string CurrentMailBoxTitle { get; set; }
        public int CurrentMailBox { get; set; }

        public ComposeMessageViewModel Message { get; set; }
    }

    
}