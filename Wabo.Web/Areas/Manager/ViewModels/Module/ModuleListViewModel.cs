﻿using System.Collections.Generic;

namespace Wabo.Web.Areas.Manager.ViewModels.Module
{
    public class ModuleListViewModel
    {
        public IEnumerable<Dal.Module> Modules { get; set; }
        public bool CanWrite { get; set; }
        public bool CanDelete { get; set; }
    }
}