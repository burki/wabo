﻿
using System.Collections.Generic;
namespace Wabo.Web.Areas.Manager.ViewModels.Group
{
    public class GroupUserAddViewModel
    {
        public IEnumerable<GroupAddUserItem> Users { get; set; }
        public Dal.Group Group { get; set; }
    }

    public class GroupAddUserItem
    {
        public Dal.User User { get; set; }
        public bool Added { get; set; }
    }
}