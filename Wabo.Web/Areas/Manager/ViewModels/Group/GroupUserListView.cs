﻿using System.Collections.Generic;

namespace Wabo.Web.Areas.Manager.ViewModels.Group
{
    public class GroupUserListView
    {
        public IEnumerable<Dal.User> Users { get; set; }
        public Dal.Group Group;
    }
}