﻿using System.Collections.Generic;

namespace Wabo.Web.Areas.Manager.ViewModels.Group
{
    public class GroupListViewModel
    {
        public IEnumerable<Dal.Group> Groups { get; set; }
        public bool CanDelete { get; set; }
        public bool CanWrite { get; set; }
    }
}