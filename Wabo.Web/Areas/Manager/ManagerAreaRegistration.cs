﻿using System.Web.Mvc;
using Wabo.Dal.Repository;
using Wabo.Web.Areas.Manager.Core;

namespace Wabo.Web.Areas.Manager
{
    public class ManagerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Manager";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            
            //ViewEngines.Engines.Clear();
            var systemSettings = new WaboEFSystemSettingsRepository();            
            var waboViewEngine = new WaboViewEngine(systemSettings.Theme);
            ViewEngines.Engines.Add(waboViewEngine);
              
            context.MapRoute(
               "RemoveUser",
               "Manager/{controller}/{action}/{group}/{user}",
               new
               {
                   controller = "Group",
                   action = "RemoveUser"
               }
            );
            context.MapRoute(
               "Manager_Message_Paging",
               "Manager/{controller}/{action}/{id}/{page}",
               new { controller = "User", action = "MailBox" }
           );
            context.MapRoute(
                "Manager_default",
                "Manager/{controller}/{action}/{id}",
                new { controller="Dashboard",action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
