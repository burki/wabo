﻿

namespace Wabo.Web.Areas.Manager.Models
{
    public class WaboSystemMenuItem
    {
        public string Title { get; set; }
        public string ControllerName { get; set; }
        public bool Selected { get; set; }        
    }
}