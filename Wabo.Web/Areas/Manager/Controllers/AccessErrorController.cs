﻿using Wabo.Web.Areas.Manager.Core;
using System.Web.Mvc;
using Wabo.Web.Areas.Manager.ViewModels;

namespace Wabo.Web.Areas.Manager.Controllers
{
    /*
     * Used for user access errors
     */
    public class AccessErrorController : WaboController
    {        

        public ActionResult Index()
        {            
            return View(TempData[AccessErrorViewModel.TempdataParameterName]);
        }

    }
}
