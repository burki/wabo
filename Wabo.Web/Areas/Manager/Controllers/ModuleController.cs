﻿using Wabo.Dal;
using Wabo.Dal.Interface;
using Wabo.Web.Areas.Manager.Core;
using Wabo.Web.Areas.Manager.Core.Attributes;
using System.Web.Mvc;
using Wabo.Web.Areas.Manager.ViewModels.Module;
using Wabo.Web.Resources;
using System;

namespace Wabo.Web.Areas.Manager.Controllers
{
    [UserAuthorized]
    public class ModuleController : WaboController
    {        
        [ModuleListPermission]
        public ActionResult Index()
        {
            bool userHasWritePermission = ModuleContext.UserCanWrite(SessionManager.AuthorizedUser(), ModuleContext.Get("Module"));
            bool userHasDeletePermission = ModuleContext.UserCanDelete(SessionManager.AuthorizedUser(), ModuleContext.Get("Module"));
            var model = new ModuleListViewModel
            {
                Modules = ModuleContext.Modules,
                CanDelete = userHasDeletePermission,
                CanWrite = userHasWritePermission
            };
            LogEvent(LogOperation.View);
            object messageTokenObject = GetActionMessageToken();
            if (messageTokenObject != null)
            {
                AddInfoMessage((string)messageTokenObject);
                ClearActionMessageToken();
            }
            return View(model);
        }

        [ModuleWritePermission]
        public ActionResult Edit(long id)
        {
            Module m = ModuleContext.Get(id);
            LogEvent(LogOperation.View,string.Format("Module:{0}",m.title));
            return View(m);
        }

        [HttpPost]
        [ModuleWritePermission]
        public ActionResult Edit(Module m)
        {
            if (ModelState.IsValid)
            {
                ModuleContext.Update(m);
                LogEvent(LogOperation.Modify,string.Format("Module:{0}",m.title));
                AddActionMessageToken(String.Format(Messages.DISP_ModuleUpdatedInfoMessage, m.title));
                return RedirectToAction("Index");                
            }
            return View(m);
        }

        [ModuleWritePermission]
        public ActionResult Create()
        {
            LogEvent(LogOperation.View);
            return View();
        }
        [HttpPost]
        [ModuleWritePermission]
        public ActionResult Create(Module m)
        {
            if (ModelState.IsValid)
            {
                ModuleContext.Add(ref m);
                LogEvent(LogOperation.Create,string.Format("Module:{0}",m.title));
                AddActionMessageToken(String.Format(Messages.DISP_ModuleCreatedInfoMessage, m.title));
                return RedirectToAction("Index");
            }
            return View(m);
        }
        
        [ModuleDeletePermission]
        public ActionResult Delete(long id)
        {
            var model = ModuleContext.Get(id);
            LogEvent(LogOperation.View,string.Format("Module:{0}",model.title));
            return View(model);
        }

        [HttpPost]
        [ModuleDeletePermission]
        public ActionResult Delete( Module model)
        {
            if( model.id > 0 )
            {                
                string moduleName = ModuleContext.Get(model.id).title;
                LogEvent(LogOperation.Delete,string.Format("Module:{0}",moduleName ));
                AddActionMessageToken(String.Format(Messages.DISP_ModuleDeletedInfoMessage, moduleName));
                ModuleContext.Remove(model.id);
                return RedirectToAction("Index");
            }
            return View(model);
        }
        
    }
}
