﻿using System;
using System.Linq;
using System.Web.Mvc;
using Wabo.Dal.Interface;
using Wabo.Web.Areas.Manager.Core;
using Wabo.Web.Areas.Manager.Core.Attributes;
using Wabo.Web.Areas.Manager.ViewModels.Group;
using Wabo.Web.Resources;

namespace Wabo.Web.Areas.Manager.Controllers
{
    [UserAuthorized]
    public class GroupController : WaboController
    {
        
        [ModuleListPermission]
        public ActionResult Index()
        {
            var model = new GroupListViewModel
                {
                    Groups = GroupContext.Groups,
                    CanDelete = ModuleContext.UserCanDelete(AuthorizedUser,ModuleContext.Get("Group")),
                    CanWrite = ModuleContext.UserCanWrite(AuthorizedUser, ModuleContext.Get("Group")),
                };
            LogEvent(LogOperation.View);
            object messageTokenObject = GetActionMessageToken();
            if (messageTokenObject != null)
            {
                AddInfoMessage((string)messageTokenObject);
                ClearActionMessageToken();
            }
            return View(model);
        }

        [ModuleWritePermission]
        public ActionResult Create()
        {
            LogEvent(LogOperation.View);
            return View();
        }

        [HttpPost]
        [ModuleWritePermission]
        public ActionResult Create(Dal.Group model)
        {
            if (ModelState.IsValid)
            {
                GroupContext.Add(ref model);
                LogEvent(LogOperation.Create, string.Format("Group:{0}", model.name));
                AddActionMessageToken(String.Format(Messages.DISP_GroupCreatedInfoMessage, model.name));
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [ModuleWritePermission]
        public ActionResult Edit(long id)
        {
            var model = GroupContext.Get(id);
            LogEvent(LogOperation.View, string.Format("Group:{0}", model.name));
            return View(model);
        }

        [HttpPost]
        [ModuleWritePermission]
        public ActionResult Edit(Dal.Group model)
        {
            if (ModelState.IsValid)
            {
                GroupContext.Update(model);
                LogEvent(LogOperation.Modify, string.Format("Group:{0}", model.name));
                AddActionMessageToken(String.Format(Messages.DISP_GroupUpdatedInfoMessage, model.name));
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [ModuleDeletePermission]
        public ActionResult Delete(long id)
        {
            var model = GroupContext.Get(id);
            LogEvent(LogOperation.View, string.Format("Group:{0}", model.name));
            return View(model);
        }

        [HttpPost]
        [ModuleDeletePermission]
        public ActionResult Delete( Dal.Group model)
        {
            if( model.id > 0)
            {
                string groupName = GroupContext.Get(model.id).name;
                LogEvent(LogOperation.Delete, string.Format("Group:{0}", groupName ));
                AddActionMessageToken(String.Format(Messages.DISP_GroupDeletedInfoMessage, groupName));
                GroupContext.Remove(model.id);
                return RedirectToAction("Index");
            }
            return View(model);
        }


        [ModuleDeletePermission]
        [ModuleWritePermission]
        public ActionResult Users(long id)
        {
            var model       = new GroupUserAddViewModel
                                  {
                                      Group = GroupContext.Get(id)
                                  };
            var groupUsers  = GroupContext.UsersInGroup(model.Group);
            model.Users = UserContext.Users.Select(usr => new GroupAddUserItem
                                                                      {
                                                                          User = usr, Added = groupUsers.Any(gu => gu.id == usr.id)
                                                                      }).ToList();
            LogEvent(LogOperation.View, string.Format("Group:{0}", model.Group.name));
            return View(model);
        }

        [HttpPost]
        [ModuleDeletePermission]
        [ModuleWritePermission]
        public ActionResult Users(GroupUserAddViewModel model)
        {
            foreach( var user in model.Users)
            {
                var usr = UserContext.Get(user.User.id);
                var grp = GroupContext.Get(model.Group.id);

                if( user.Added)
                {                    
                    LogEvent(LogOperation.Modify, string.Format("User:{0} added to Group:{1} ", usr.username, grp.name));
                    GroupContext.AddUserToGroup(model.Group,user.User);
                }
                else{                
                    LogEvent(LogOperation.Modify, string.Format("User:{0} removed from Group:{1} ", usr.username, grp.name));
                    GroupContext.RemoveUserFromGroup(model.Group,user.User);
                }
            }
            
            return RedirectToAction("Index");
        }

    }
}
