﻿using System;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using Wabo.Dal.Interface;
using Wabo.Web.Areas.Manager.Core;
using Wabo.Web.Areas.Manager.Core.Attributes;
using Wabo.Web.Resources;
using Wabo.Dal;
using Wabo.Web.Areas.Manager.ViewModels;
using Wabo.Web.Areas.Manager.ViewModels.Log;
using System.Data.Objects;

namespace Wabo.Web.Areas.Manager.Controllers
{
    [UserAuthorized]
    public class LogViewerController : WaboController
    {
        private const string SessionFilterKey = "LOG_VIEW_FILTER_OBJECT";
        private const int AllUsersFilterValue = -1;

        [ModuleListPermission]        
        public ActionResult Index()
        {
            return RedirectToActionPermanent("List");
        }
        [ModuleListPermission]
        public ActionResult List(int id = 1/*page number */)
        {
            LogListViewModel filter;
            
            if (id < 1)
                id = 1;
            if (SessionManager.CheckSessionObject(SessionFilterKey))
            {
                filter = (LogListViewModel)SessionManager.ReturnSessionObject(SessionFilterKey);                
            }
            else
            {
                filter = new LogListViewModel();
                filter.FilterModule = Messages.LogListAllModulesValue;
                filter.FilterUserId = AllUsersFilterValue;
                filter.FilterStartDate = DateTime.Today.AddMonths(-1);
                filter.FilterEndDate = DateTime.Today;
                //ViewBag.CurrentLogItems = EventLogs().ToPagedList(id, SystemSettingsContext.LogViewerItemCountPerPage);
            }            

            ViewBag.CurrentLogItems = EventLogs().Where(
                s => EntityFunctions.TruncateTime(s.date) >= filter.FilterStartDate &&
                     EntityFunctions.TruncateTime(s.date) <= filter.FilterEndDate &&
                     ( s.module == filter.FilterModule  || filter.FilterModule == Messages.LogListAllModulesValue ) &&
                     (s.user_id == filter.FilterUserId || filter.FilterUserId == AllUsersFilterValue)
            ).ToPagedList(id, SystemSettingsContext.LogViewerItemCountPerPage);

            
            // Get all users for filter
            filter.Users = UserContext.Users.Select(u => new SelectListItem
            {
                Value = u.id.ToString(),
                Text = u.name+" "+u.last_name
            }).ToList();
            filter.Users.Add( new SelectListItem{
                Value = AllUsersFilterValue.ToString(),
                Text = Messages.LogListAllUsersText
            });
            // Get all modules for filter
            filter.Modules = ModuleContext.Modules.Select(x => new SelectListItem
            {
                Value = x.controller.ToString(),
                Text = x.title
            }).ToList();

            filter.Modules.Add( new SelectListItem { Value = Messages.LogListAllModulesValue, Text = Messages.LogListAllModulesText});

            LogEvent(LogOperation.View, String.Format("User {0}", AuthorizedUser.username));
            return View(filter);
        }

        [ModuleDeletePermission]
        [ModuleWritePermission]
        [HttpPost]
        public ActionResult Purge(ConfirmationViewModel model)
        {
            PurgeLogs(SystemSettingsContext.Settings.days_to_purge);
            return RedirectToAction("List");
        }

        [ModuleDeletePermission]
        [ModuleWritePermission]
        public ActionResult Purge()
        {
            ConfirmationViewModel model = new ConfirmationViewModel();
            model.CancelAction = "List";
            model.CancelController = "LogViewer";
            model.ConfirmationAction = "Purge";
            model.ConfirmationController="LogViewer";
            model.Title = Messages.LogListPurgeTitle;
            model.Message = String.Format(Messages.LogListPurgeMessage, SystemSettingsContext.Settings.days_to_purge);
            return View("ConfirmationView", model);
        }

        [ModuleListPermission]
        public ActionResult Filter(LogListViewModel filter)
        {
            if (SessionManager.CheckSessionObject(SessionFilterKey))
                SessionManager.FreeSessionObject(SessionFilterKey);            
            SessionManager.RegisterSessionObject(SessionFilterKey,filter);            
            return RedirectToAction("List");
        }

    }
}
