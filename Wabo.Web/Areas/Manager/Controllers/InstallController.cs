﻿using System;
using Wabo.Web.Areas.Manager.Core;
using System.Web.Mvc;
using Wabo.Web.Areas.Manager.ViewModels;
using System.Linq;

namespace Wabo.Web.Areas.Manager.Controllers
{
    public class InstallController : WaboController
    {
       
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Truncates base module tables, 
        /// creates two users :  admin and manager
        /// creates two groups : administrators and managers
        /// Gives all permissions to administrators
        /// Use with care :)
        /// </summary>      
        [HttpPost]
        public ActionResult Do(SetupOptionsViewModel model)
        {

            ModuleContext.RemoveAll();
            GroupContext.RemoveAll();                
            UserContext.RemoveAll();
            SystemSettingsContext.Clear();

            var settings = new Dal.SystemSettings
                               {
                                   application_name = "WABO - MVC BOOTSTRAP",
                                   log_items_per_page = 20,
                                   login_lock_timeout = 300,
                                   max_login_attempts = 5,
                                   theme = "Default"
                               };
            SystemSettingsContext.Save(settings);

            var usr = UserContext.CreateNewUserObject("manager", "manager", "manager", "info@kai-tribe.com");
            UserContext.Add(ref usr);
            usr = UserContext.CreateNewUserObject("admin", "admin", "admin", "info@kai-tribe.com");
            UserContext.Add(ref usr); 
            

            var managerGroup = new Dal.Group {name = "Managers"};
            GroupContext.Add(ref managerGroup);

            usr = UserContext.Get("manager");
            if( usr != null )
                GroupContext.AddUserToGroup(managerGroup, usr);

            var adminGroup = new Dal.Group {name = "Administrators"};
            GroupContext.Add(ref adminGroup);
            
            var adminUser = UserContext.Get("admin");
            if (adminUser != null)
                GroupContext.AddUserToGroup(adminGroup, adminUser);

            
            var module = new Dal.Module
            {
                title = "User Management",
                controller = "User",
                order = 1,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if( adminGroup != null )
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);
                
            module = new Dal.Module
            {
                title = "User Groups",
                controller = "Group",
                order = 2,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);

            module = new Dal.Module
            {
                title = "Permissions",
                controller = "Acl",
                order = 100,
                show_on_menu = false
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);

            module = new Dal.Module
            {
                title = "Log Viewer",
                controller = "LogViewer",
                order = 90,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);

            module = new Dal.Module
            {
                title = "Modules",
                controller = "Module",
                order = 4,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);

            module = new Dal.Module
            {
                title = "Dashboard",
                controller = "Dashboard",
                order = 0,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);

            module = new Dal.Module
            {
                title = "System Settings",
                controller = "SystemSettings",
                order = 1,
                show_on_menu = true
            };
            ModuleContext.Add(ref module);
            if (adminGroup != null)
                ModuleContext.SetModuleGroupPermissions(adminGroup, module, true, true, true, true);
                                                 
            return RedirectToAction("Index", "Dashboard");
            
            //return View();
        }

    }
}
