﻿using Wabo.Web.Areas.Manager.Core;
using Wabo.Web.Areas.Manager.Core.Attributes;
using System.Web.Mvc;
using Wabo.Web.Areas.Manager.ViewModels;

namespace Wabo.Web.Areas.Manager.Controllers
{
    [UserAuthorized]
    public class DashboardController : WaboController
    {
        //
        // GET: /Manager/Dashboard/
        [ModuleListPermission]
        public ActionResult Index()
        {
            var adminDashboard = new DashboardViewModel();
            return View(adminDashboard);
        }

    }
}
