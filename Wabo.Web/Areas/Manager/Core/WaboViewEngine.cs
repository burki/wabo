﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wabo.Web.Areas.Manager.Core
{
    public class WaboViewEngine:RazorViewEngine
    {
        public static string ThemesBasePath = "~/Content/Themes/Manager/";
        
        public WaboViewEngine(string Theme)
        {
            string[] viewLocationFormatArr = new string[3];
            viewLocationFormatArr[0] = ThemesBasePath + Theme + "/views/{1}/{0}.cshtml";            
            viewLocationFormatArr[1] = ThemesBasePath + Theme + "/views/Shared/{1}/{0}.cshtml";
            viewLocationFormatArr[2] = ThemesBasePath + Theme + "/views/Shared/{0}.cshtml";
            AreaViewLocationFormats = ViewLocationFormats = viewLocationFormatArr;

            string[] masterLocationFormatArr = new string[3];
            masterLocationFormatArr[0] = ThemesBasePath + Theme + "/views/{1}/{0}.cshtml";
            masterLocationFormatArr[1] = ThemesBasePath + Theme + "/views/Shared/{1}/{0}.cshtml";
            masterLocationFormatArr[2] = ThemesBasePath + Theme + "/views/Shared/{0}.cshtml";            
            AreaMasterLocationFormats = MasterLocationFormats = masterLocationFormatArr;
            

            string[] partialViewLocationFormatArr = new string[2];
            partialViewLocationFormatArr[0] = ThemesBasePath + Theme + "/views/{1}/{0}.cshtml";
            partialViewLocationFormatArr[1] = ThemesBasePath + Theme + "/views/Shared/{1}/{0}.cshtml";
            partialViewLocationFormatArr[1] = ThemesBasePath + Theme + "/views/Shared/{0}.cshtml";
            AreaPartialViewLocationFormats = PartialViewLocationFormats  = partialViewLocationFormatArr;
            //AppendLocationFormats(partialViewLocationFormatArr, PartialViewLocationFormats);

        }

        private string[] AppendLocationFormats(string[] newLocations, string[] defaultLocations)
        {
            var viewLocations = new List<string>();
            viewLocations.AddRange(newLocations);
            viewLocations.AddRange(defaultLocations);
            return viewLocations.ToArray();
        }

        public override ViewEngineResult FindPartialView(ControllerContext controllerContext, string partialViewName, bool useCache)
        {
            ViewEngineResult r = base.FindPartialView(controllerContext, partialViewName, useCache);
            return r;
        }
    }
}