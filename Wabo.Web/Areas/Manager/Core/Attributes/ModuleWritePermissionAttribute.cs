﻿
namespace Wabo.Web.Areas.Manager.Core.Attributes
{
    public class ModuleWritePermissionAttribute:WaboPermissionAttribute
    {
        public ModuleWritePermissionAttribute(bool byPass = false)
            : base(byPass, new Exceptions.NoWritePermissionException() )
        {
        }

        
    }
}