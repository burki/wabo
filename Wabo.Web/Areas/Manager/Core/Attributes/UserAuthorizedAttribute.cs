﻿
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Wabo.Web.Areas.Manager.Core.Attributes
{
    public class UserAuthorizedAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return SessionManager.IsAuthorized();
        }
        
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            if( !SessionManager.IsAuthorized() )
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Logon", action = "Index" }));
            }
        }
        
    }

}