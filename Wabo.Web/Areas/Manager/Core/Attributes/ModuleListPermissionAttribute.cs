﻿

namespace Wabo.Web.Areas.Manager.Core.Attributes
{
    public class ModuleListPermissionAttribute : WaboPermissionAttribute
    {        

        public ModuleListPermissionAttribute(bool byPass=false)
            : base(byPass,new Exceptions.NoListPermissionException())
        {            
        }        
    }
}