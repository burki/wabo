﻿
namespace Wabo.Web.Areas.Manager.Core.Attributes
{
    public class ModuleReadPermissionAttribute : WaboPermissionAttribute
    {
        public ModuleReadPermissionAttribute(bool byPass=false)
            : base(byPass, new Exceptions.NoReadPermissionException())
        {
        }
    }
}