﻿

namespace Wabo.Web.Areas.Manager.Core.Attributes
{
    public class ModuleDeletePermissionAttribute:WaboPermissionAttribute
    {
        public ModuleDeletePermissionAttribute(bool byPass=false)
            : base(byPass, new Exceptions.NoDeletePermissionException())
        {
        }
    }
}