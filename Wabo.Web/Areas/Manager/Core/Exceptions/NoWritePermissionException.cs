﻿using System;

namespace Wabo.Web.Areas.Manager.Core.Exceptions
{
    public class NoWritePermissionException:Exception
    {
        public NoWritePermissionException()
            : base(Resources.Messages.NoDeletePermissionError)
        {
        }
    }
}