﻿using System;


namespace Wabo.Web.Areas.Manager.Core.Exceptions
{
    public class NoListPermissionException : Exception
    {
        public NoListPermissionException():base(Resources.Messages.NoListPermissionError)
        {            
        }
    }
}