﻿using System;


namespace Wabo.Web.Areas.Manager.Core.Exceptions
{
    public class NoDeletePermissionException:Exception
    {
        public NoDeletePermissionException():base(Resources.Messages.NoDeletePermissionError)
        {
        }
    }
}