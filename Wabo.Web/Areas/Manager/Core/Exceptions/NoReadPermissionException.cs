﻿using System;


namespace Wabo.Web.Areas.Manager.Core.Exceptions
{
    public class NoReadPermissionException:Exception
    {
        public NoReadPermissionException()
            : base(Resources.Messages.NoReadPermissionError)
        {
        }
    }
}