﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Wabo.Web.Areas.Manager.Core;
using Wabo.Web.Controllers;

namespace Wabo.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            MetadataConfig.RegisterBuddyClasses();
        }
        /*
        protected void Application_Error( object sender , EventArgs e )
        {
            Exception exp = Server.GetLastError();

            Response.Clear();
            
            var httpException = exp as HttpException;
            
            var routeData = new RouteData();
            routeData.Values.Add("controller","Error");
            
            if( httpException == null )
                routeData.Values.Add("action","Index");
            else
            {
                
                switch (httpException.GetHttpCode() )
                {
                    case 404: // Page not found
                        routeData.Values.Add("action","Http404");
                        break;
                    case 500: // Server error
                        routeData.Values.Add("action","Http500");
                        break;
                    default:
                        routeData.Values.Add("action","Index");
                        break;
                }

            }
            routeData.Values.Add("error",exp);
            Server.ClearError();
            IController controller = new ErrorController();
            controller.Execute( new RequestContext(new HttpContextWrapper(Context),routeData ) );

        }
         * */
    }



}