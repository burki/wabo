﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wabo.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult Http404(Exception error)
        {
            Response.StatusCode = 404;
            
            return View();
        }

        public ActionResult Http500(Exception error)
        {
            Response.StatusCode = 500;
            return View(error);
        }

    }
}
